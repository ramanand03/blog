from django.shortcuts import render, redirect
from .models import Post
from comment.models import Comment
from article.models import Article
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User

# Create your views here.

def showAllPosts(request):    
    post = Post.objects.select_related('article')
    return render(request, 'post/allpost.html', {'post':post, 'title':'Posts', 'allpost_button':'active'})


def articleAllPosts(request, article_title):
    post = Post.objects.select_related('article').filter(article_id = Article.objects.get(title = article_title).id)
    return render(request, 'post/posts-article.html', {'post':post, 'article_title':article_title, 'title':article_title+' Posts'})


def viewPost(request, post_id):
    post = Post.objects.select_related('article').get(id = post_id)
    flag = request.user.id == post.user_id  # flag to hide DELETE & EDIT button
    comments = Comment.objects.select_related('user').filter(post_id = post_id)
    if request.method == 'POST':
        input_comment = request.POST['comment']
        Comment(comment = input_comment, user_id = request.user.id, post_id = post_id).save()
    return render(request, 'post/view.html', {'post':post, 'comments':comments, 'flag':flag, 'title':post.title})


@login_required
def createPost(request):       
    if request.method == 'POST':
        Post(title = request.POST['post_title'], content = request.POST['post_content'], article_id = Article.objects.get(title = request.POST['post_article']).id, user_id = request.user.id).save()
        return redirect("/posts/"+ request.POST['post_article'] )
    article = Article.objects.all()
    return render(request, 'post/create-post.html', {'article':article, 'create_post_displayflag':'none', 'title':'Create Post'})

@login_required
def editPost(request, post_id):
    if request.user.id == Post.objects.get(id = post_id).user_id:
        if request.method == 'POST':
            post = Post.objects.get(id = post_id)
            post.title = request.POST['post_title']
            post.content = request.POST['post_content']
            post.save()
            return redirect("/posts/")
    else:
        return redirect("/")
    post = Post.objects.get(id = post_id)
    return render(request, 'post/edit-post.html', {'post':post, 'create_post_displayflag':'none', 'title':'Edit Post'})

@login_required
def deletePost(request, post_id):
    if request.user.id == Post.objects.get(id = post_id).user_id:
        Post.objects.get(id = post_id).delete()
        return redirect("/posts")
    return redirect("/")