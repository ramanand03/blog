from django.db import models
from django.contrib.auth.models import User
from article.models import Article

# Create your models here.

class Post(models.Model):
    id = models.AutoField(primary_key=True)
    content = models.CharField(max_length=500)
    title = models.CharField(max_length=100)
    article = models.ForeignKey(Article, on_delete = models.CASCADE)
    user = models.ForeignKey(User, on_delete = models.CASCADE)
    class Meta:
        db_table = "post"