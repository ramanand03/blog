from django.urls import path,include
# import article.urls, post.urls

from post import views 

urlpatterns = [
    path('', views.showAllPosts, name='posts'),
    path('view/<int:post_id>/', views.viewPost),
    path('view/<int:post_id>/delete/', views.deletePost),
    path('view/<int:post_id>/edit/', views.editPost),
    path('<str:article_title>/', views.articleAllPosts, name='posts')

]