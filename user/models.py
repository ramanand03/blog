from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class Otp(models.Model):
    otp = models.IntegerField()
    user = models.ForeignKey(User, on_delete = models.CASCADE)
    time = models.DateTimeField(auto_now=True)
    class Meta:
        db_table = "otp"

