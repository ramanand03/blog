from django.shortcuts import render,redirect
from .form import RegisterForm
from post.models import Post

# Create your views here.

def index(request):
    post = Post.objects.select_related('article').all()[:2]
    return render(request, 'index.html', {'post':post, 'title':'Blog App', 'home_button':'active'})


def createUser(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/login')
    else:
        form = RegisterForm()        
    return render(request, 'user/register.html', {'form':form, 'title':'Register User'})

