from django.urls import path

from . import views as v

urlpatterns = [
    path('register/', v.Register.as_view()),
    path('login/', v.Login.as_view()),
    path('password-reset/', v.PasswordReset.as_view()),

    path('password-forget/generate-otp/', v.PasswordForgetGenerateOtp.as_view()),
    path('password-forget/verify-otp/', v.PasswordForgetVerifyOtp.as_view()),
    path('password-forget/', v.PasswordForget.as_view()),

    path('articles/', v.Articles.as_view()),
    path('articles/<slug:article_title>/', v.ArticlesById.as_view()),
    path('posts/', v.Posts.as_view()),
    path('posts/<int:pk>/', v.PostsById.as_view()),
    path('comments/post/<int:post_id>/', v.Comments.as_view()),
    path('comments/<int:pk>/', v.CommentsEditDelete.as_view())
    ]