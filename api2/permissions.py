from rest_framework import permissions

from django.contrib.auth.models import User
    

class IsUserLogin(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            return True
        else:
            if request.user.is_superuser:
                return True
            else:
                return (User.objects.filter(username=request.user).count() == 1)

