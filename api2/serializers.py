from rest_framework import serializers
from rest_framework_jwt.settings import api_settings
from django.contrib.auth import authenticate
from django.contrib.auth.models import update_last_login

from post.models import Post
from user.models import Otp
from article.models import Article
from comment.models import Comment 
from django.contrib.auth.models import User

from random import randint
from datetime import datetime


JWT_PAYLOAD_HANDLER = api_settings.JWT_PAYLOAD_HANDLER
JWT_ENCODE_HANDLER = api_settings.JWT_ENCODE_HANDLER


class RegisterSerializer(serializers.Serializer):
    username = serializers.CharField( write_only=True)
    email = serializers.EmailField( write_only=True)
    password = serializers.CharField( write_only=True)
    user = serializers.DictField(read_only=True)
    token = serializers.CharField(max_length=255, read_only=True)

    def validate(self, data):        
        if User.objects.filter(username=data['username']).exists():
            raise serializers.ValidationError("A User is already registered with this username!")        
        
        if User.objects.filter(email=data['email']).exists():
            raise serializers.ValidationError("A User is already registered with this email!")        
        return data 

    def create(self, validated_data):        
        user = User.objects.create_user(username=validated_data['username'], email=validated_data['email'], password=validated_data['password'])
        payload = JWT_PAYLOAD_HANDLER(user)
        jwt_token = JWT_ENCODE_HANDLER(payload)
        return {'user':{'id':user.id, 'username':user.username, 'email':user.email}, 'token': jwt_token}

 

class LoginSerializer(serializers.Serializer):
    username = serializers.CharField( write_only=True)
    password = serializers.CharField( write_only=True)
    user = serializers.DictField(read_only=True)
    token = serializers.CharField(max_length=255, read_only=True)
    
    def validate(self, data):
        user = authenticate(username= data["username"], password=data["password"]) 
        if user is None:
            raise serializers.ValidationError('A user with this username & password not found!')
        try:
            payload = JWT_PAYLOAD_HANDLER(user)
            jwt_token = JWT_ENCODE_HANDLER(payload)
            update_last_login(None, user)
        except User.DoesNotExist:
            raise serializers.ValidationError( 'User with given username and password does not exists!')
        return {
            'user': {'id': user.id, 'username': user.username, 'email': user.email},
            'token': jwt_token
        }



class ResetPasswordSerializer(serializers.Serializer):
    old_password = serializers.CharField(write_only=True)
    new_password = serializers.CharField(write_only=True)
    
    def validate(self, data):
        user = self.context['user']
        if not user.check_password(data['old_password']):
            raise serializers.ValidationError('Your old password is wrong!')
        elif data['old_password'] == data['new_password']:
            raise serializers.ValidationError("Your new password can't be same as your old password!")        
        user.set_password(data['new_password'])
        user.save()
        return data




class ArticleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Article
        fields = '__all__'
        extra_kwargs = {'user': {'write_only': True,} }
    
    def update(self, article, validated_data):
        article.title = validated_data.get('title', article.title)
        article.desc = validated_data.get('desc', article.desc)
        article.save()
        return article



class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = '__all__'
        extra_kwargs = {'user': {'write_only': True,} }   

    def update(self, posts, validated_data):
        posts.title = validated_data.get('title', posts.title)
        posts.content = validated_data.get('content', posts.content)
        posts.save()
        return posts



class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = '__all__'

    def update(self, comment, validated_data):
        comment.comment = validated_data.get('comment', comment.comment)
        comment.save()
        return comment


class PasswordForgetGenerateOtpSerializer(serializers.Serializer):
    email = serializers.EmailField( write_only=True)
    user = serializers.DictField( read_only=True)
    otp = serializers.IntegerField( read_only=True)

    def validate(self, data):
        if not User.objects.filter(email = data['email']).exists():
            raise serializers.ValidationError("you are not register.")
        return data

    def create(self,validated_data):
        otp = randint(100000, 999999)
        validated_data['otp'] = otp
        user_id = User.objects.get(email=validated_data['email']).id

        if Otp.objects.filter(user_id = user_id).exists():
            instance = Otp.objects.get(user_id = user_id)
            instance.otp = otp
            instance.save()
        else:
            Otp.objects.create( otp=otp, user_id = user_id)
        
        return {'user': {'id': user_id}, 'otp': otp}



class PasswordForgetVerifyOtpSerializer(serializers.Serializer):
    user_id = serializers.IntegerField(write_only=True)
    otp = serializers.IntegerField( write_only=True)
    

    def validate(self, data):
        if not User.objects.filter(id = data['user_id']).exists():
            raise serializers.ValidationError("you are not register.")
        if not Otp.objects.filter(id = data['user_id']).exists():
            raise serializers.ValidationError("Generate Otp First.")
        if data['otp'] != Otp.objects.get(id = data['user_id']).otp:
            raise serializers.ValidationError("Wrong Otp")

        otp_generate_time = Otp.objects.get(id = data['user_id']).time
        current_time = datetime.now()
        timestamp = ( current_time - otp_generate_time).seconds
        if timestamp > 300:
            raise serializers.ValidationError("otp expired, generate new")
        return data

class PasswordForgetSerializer(serializers.Serializer):
    user_id = serializers.IntegerField(write_only=True)
    password = serializers.CharField(write_only=True)

    def validate(self, data):
        if len(data['password']) < 8:
            raise serializers.ValidationError('Password length must be atleast 8 characters')
        user = User.objects.get(id = data['user_id'])
        user.set_password(data['password'])
        user.save()
        return data