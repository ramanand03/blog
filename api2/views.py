from rest_framework.views import APIView
from rest_framework.response import Response

# import permissions
from rest_framework.permissions import IsAuthenticated, AllowAny
from .permissions import IsUserLogin

# import serializer
from .serializers import (CommentSerializer,ArticleSerializer, PostSerializer, RegisterSerializer, 
            LoginSerializer, ResetPasswordSerializer, PasswordForgetGenerateOtpSerializer, PasswordForgetVerifyOtpSerializer, PasswordForgetSerializer)

# import models
from post.models import Post
from user.models import Otp
from article.models import Article
from comment.models import Comment
from django.contrib.auth.models import User

# default exception
from rest_framework.views import exception_handler
from rest_framework.exceptions import NotAuthenticated, MethodNotAllowed

# email
from django.core.mail import send_mail
from django.conf import settings  

# function for return response message
def responseMsg(success, status_code, msg, data):
    if not isinstance(msg, str):    
        if "non_field_errors" in msg:
            msg = msg['non_field_errors'][0]
            # giving list msg['non_field_errors'].
        else:
            msg = "Required Field OR Value missing."
    return {
        "success": success,
        "status code": status_code,
        "message": msg,
        "data": data
    }


# custom exception handler
def custom_exception_handler(exc, context):
    if isinstance(exc, NotAuthenticated):
        return Response(responseMsg(False, 401, "User Not Login", {}) )
    if isinstance(exc, MethodNotAllowed):
        return Response(responseMsg(False, 401, "Method Not Allowed", {}) )
            

# user register view
class Register(APIView):

    

    permission_classes = (AllowAny,)
    def post(self, request):
        serializer = RegisterSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            
            email_title = "Congratulations! Your account created successfully."
            email_message = "Hey, "+serializer.data['user']['username']+"\n\n"+"Thank you for creating your account."
            email_to = serializer.data['user']['email'].split(' ') 
            
            send_mail( email_title, email_message, settings.EMAIL_HOST_USER, email_to, fail_silently=False)
            return Response(responseMsg(True, 201, "User Register successfully", serializer.data) )
        
        return Response(responseMsg(False, 400, serializer.errors, {}) )


# user login view
class Login(APIView):
    permission_classes = (AllowAny,)
    def post(self, request):
        serializer = LoginSerializer(data=request.data)
        if serializer.is_valid():
            return Response(responseMsg(True, 202, "Successfully Login", serializer.data) )
        return Response(responseMsg(False, 400, serializer.errors, {}) )
        

# user password-reset view
class PasswordReset(APIView):
    permission_classes = (IsAuthenticated, )
    def post(self, request):
        serializer = ResetPasswordSerializer(data=request.data, context={'user':request.user})
        if serializer.is_valid():
            return Response(responseMsg(True, 202, "Password Reset Successfully", {}) )
        return Response(responseMsg(False, 400, serializer.errors, {}) )


# article view
class Articles(APIView):    
    permission_classes = (IsUserLogin,)
    def get(self, request):
        if request.user.is_anonymous:
            articles = Article.objects.all()
            serializer = ArticleSerializer(articles, many = True)
            return Response(responseMsg(True, 200, "Articles Found", {'other_articles': serializer.data}) )

        user_articles = Article.objects.filter(user_id = request.user.id)
        other_articles = Article.objects.exclude(user_id = request.user.id)
        serializer1 = ArticleSerializer(user_articles, many = True)
        serializer2 = ArticleSerializer(other_articles, many = True)
        data = {'user_articles': serializer1.data, 'other_articles': serializer2.data}
        return Response (responseMsg(True, 200, "articles Found", data))


    def post(self, request):        
        data = request.data.copy()
        data['user'] = request.user.id
        serializer = ArticleSerializer(data = data)
        if serializer.is_valid():

            if Article.objects.filter(title=request.data['title']).exists():
                return Response(responseMsg(False, 501, "Article is already Created", {}) )
            serializer.save()
            return Response(responseMsg(True, 201, "Article Created Successfully", serializer.data) )
        return Response(responseMsg(False, 406, serializer.errors, {}) )


# articlebyID view
class ArticlesById(APIView):
    permission_classes = (IsUserLogin, )
    def get(self, request, article_title):
        if not Article.objects.filter(title=article_title).exists():
            return Response(responseMsg(False, 501, "Article Not Found", {}) )

        if not Post.objects.filter(article_id=Article.objects.get(title = article_title).id).exists():
            return Response(responseMsg(False, 501, "Post Not Found", {}) )
                
        posts = Post.objects.filter(article_id=Article.objects.get(title = article_title).id)
        serializer = PostSerializer(posts, many= True)
        return Response(responseMsg(True, 200, "Posts Found", serializer.data) )
            

    def put(self, request, article_title):        
        if request.user.is_superuser :
            if not Article.objects.filter(title=article_title).exists():
                return Response(responseMsg(False, 501, "Article not Found", {}) )
                    
            article = Article.objects.get(title=article_title)
            data = request.data.copy()
            data['user'] = article.user_id
            serializer = ArticleSerializer(article, data=data)

            if serializer.is_valid():
                serializer.save()
                return Response(responseMsg(True, 200, "Article Updated Successfully", serializer.data) )
            return Response(responseMsg(False, 406, serializer.errors, {}) )
        return Response(responseMsg(False, 501, "You are not Authorized", {}) )
            

    def delete(self, request, article_title):
        if request.user.is_superuser :
            if not Article.objects.filter(title=article_title).exists():
                return Response(responseMsg(False, 501, "Article Not Found", {}) )
                    
            Article.objects.get(title=article_title).delete()
            return Response(responseMsg(True, 200, "Article deleted successfully", {}) )
        return Response(responseMsg(False, 501, "You are not Authorized", {}) )
 

# post view
class Posts(APIView):    
    permission_classes = (IsUserLogin,)
    def get(self, request):
        if request.user.is_anonymous:
            posts = Post.objects.all()
            serializer = PostSerializer(posts, many = True)
            return Response (responseMsg(True, 200, "Posts Found", {'other_post': serializer.data}) )
        
        user_posts = Post.objects.filter(user_id = request.user.id)
        other_posts = Post.objects.exclude(user_id = request.user.id)
        serializer1 = PostSerializer(user_posts, many = True)
        serializer2 = PostSerializer(other_posts, many = True)
        data = {'user_posts': serializer1.data, 'other_posts': serializer2.data}
        return Response (responseMsg(True, 200, "Posts Found", data))

    def post(self, request):
        data = request.data.copy()
        data['user'] = request.user.id
        serializer = PostSerializer(data = data)
        if serializer.is_valid():
            serializer.save()
            return Response(responseMsg(True, 201, "Post Created Successfully", serializer.data))
        return Response(responseMsg(False, 406, serializer.errors, {}))
            

# articleByID view
class PostsById(APIView):
    permission_classes = (IsUserLogin, )
    def get(self, request, pk):
        try:
            posts = Post.objects.get(pk=pk)
        except:
            return Response(responseMsg(False, 501, "Post Not Found", {}))   
        serializer = PostSerializer(posts)
        return Response(responseMsg(True, 200, "Post Found", serializer.data))
    

    def put(self, request, pk):        
        if request.user.is_superuser or request.user.id == Post.objects.get(pk=pk).user_id:
            try:
                posts = Post.objects.get(pk=pk)
            except:
                return Response(responseMsg(False, 501, "Post Not Found", {}))
            data = request.data.copy()
            data['user'] = posts.user_id
            data['article'] = posts.article_id
            serializer = PostSerializer(posts, data = data)
            
            if serializer.is_valid():
                serializer.save()
                return Response(responseMsg(True, 200, "Post Updated Successfully", serializer.data))
            return Response(responseMsg(False, 406, serializer.errors, {}))
        return Response(responseMsg(False, 401, "You are not Authorized", {}))
    

    def delete(self, request, pk):
        if request.user.is_superuser or request.user.id == Post.objects.get(pk=pk).user_id:
            try:
                posts = Post.objects.get(pk=pk)
            except:
                return Response(responseMsg(False, 501, "Post NotFound", {}))  
            posts.delete()
            return Response(responseMsg(True, 200, "Post Deleted", {})) 
        return Response(responseMsg(False, 401, "You are not Authorized", {}))
           
    
# comments view
class Comments(APIView):    
    permission_classes = (IsUserLogin,)
    def get(self, request, post_id):
        if not Post.objects.filter(id=post_id).exists():
            return Response(responseMsg(False, 501, "Post NOt Found", {}) )
        if not Comment.objects.filter(post_id=post_id).exists():  
            return Response(responseMsg(True, 200, "Comments Not Found", {}) )

        if request.user.is_anonymous:                    
            comments = Comment.objects.filter(post_id=post_id)
            serializer = CommentSerializer(comments, many = True)
            return Response(responseMsg(True, 200, "Comments Found", serializer.data) )
        user_comments = Comment.objects.filter(user_id = request.user.id)
        other_comments = Comment.objects.exclude(user_id = request.user.id)
        serializer1 = CommentSerializer(user_comments, many = True)
        serializer2 = CommentSerializer(other_comments, many = True)
        data = {'user_comments': serializer1.data, 'other_comments': serializer2.data}
        return Response (responseMsg(True, 200, "comments Found", data))

    def post(self, request, post_id):
        data = request.data.copy()
        data['user'] = request.user.id
        data['post'] = post_id
        serializer = CommentSerializer(data = data)
        if serializer.is_valid():
            serializer.save()
            return Response(responseMsg(True, 201, "Comment Successfully", serializer.data) )
        return Response(responseMsg(False, 406, serializer.errors, {}) )


# CommentByID view
class CommentsEditDelete(APIView):
    permission_classes = (IsUserLogin,)
    def get(self, request, pk):
        if not Comment.objects.filter(id=pk).exists():
            return Response(responseMsg(False, 501, "Comments not Found", {}) )
        comments = Comment.objects.get(id=pk)
        serializer = CommentSerializer(comments)
        return Response(responseMsg(True, 200, "Comments Found", serializer.data) )
          
    
    def put(self, request, pk):
        if request.user.is_superuser:
            if not Comment.objects.filter(id=pk).exists():
                return Response(responseMsg(False, 501, "Comment not Found", {}) )
            
            data = request.data.copy()
            comment = Comment.objects.get(id=pk)
            data['user'] = comment.user_id
            data['post'] = comment.post_id
            serializer = CommentSerializer(comment, data = data)
            if serializer.is_valid():
                serializer.save()
                return Response(responseMsg(True, 201, "Comment Edit Successfull", serializer.data) )
            return Response(responseMsg(False, 501, serializer.errors, {}) )        
        return Response(responseMsg(False, 501, "you are not Authorized", {}) )

    def delete(self, request, pk):
        if request.user.is_superuser:
            if not Comment.objects.filter(id=pk).exists():
                return Response(responseMsg(False, 501, "Comment Found", {}) )
                     
            Comment.objects.get(pk=pk).delete()
            return Response(responseMsg(True, 201, "Comment Delete Success", {}) )
        return Response(responseMsg(False, 501, "you are not Authorized", {}) )



class PasswordForgetGenerateOtp(APIView):
    permission_classes = (AllowAny,)
    def post(self, request):
        
        serializer = PasswordForgetGenerateOtpSerializer(data = request.data)
        if serializer.is_valid():
            serializer.save()

            email_title = "OTP for forget password."
            email_message = "OTP For Password reset is:\n"+str(serializer.data['otp'])
            email_to = request.data['email'].split(' ') 
            
            send_mail( email_title, email_message, settings.EMAIL_HOST_USER, email_to, fail_silently=False)
            return Response(responseMsg(False, 501, "otp sent successfully", {'user':serializer.data['user']}) )
        return Response(responseMsg(False, 501, serializer.errors, {}) )

class PasswordForgetVerifyOtp(APIView):
    permission_classes = (AllowAny,)
    def post(self, request):

        serializer = PasswordForgetVerifyOtpSerializer(data = request.data)
        if serializer.is_valid():
            return Response(responseMsg(False, 501, "Otp Matched", {'user': {'id':request.data['user_id']}}) )
        return Response(responseMsg(False, 501, serializer.errors, {}) )


class PasswordForget(APIView):
    permission_classes = (AllowAny,)
    def post(self, request):
        serializer = PasswordForgetSerializer(data = request.data)
        if serializer.is_valid():
            return Response(responseMsg(False, 501, "Password Changed Successfully", {}) )    
        return Response(responseMsg(False, 501, serializer.errors, {}) )

        