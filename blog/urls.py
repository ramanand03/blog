from django.contrib import admin
from django.urls import path, include
from django.contrib.auth import views as authentication_views

# import apps
import user.urls, post.urls,article.urls
from user import views as user_views
from post import views as post_views
from article import views as article_views
from api2 import urls as api2_urls

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', user_views.index),
    path('register/', user_views.createUser, name='register'),
    path('login/', authentication_views.LoginView.as_view(template_name='user/login.html'), name='login'),
    path('logout/', authentication_views.LogoutView.as_view(), name='logout'),
    path('articles/', include(article.urls)),
    path('posts/', include(post.urls)),
    path('user/', include(user.urls)),
    path('create/post', post_views.createPost),
    path('create/article', article_views.createArticle),
    path('api/v2/', include(api2_urls))
    
]
