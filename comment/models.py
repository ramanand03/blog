from django.db import models
from django.contrib.auth.models import User
from post.models import Post

# Create your models here.

class Comment(models.Model):
    id = models.AutoField(primary_key=True)
    comment = models.CharField(max_length=100)
    user = models.ForeignKey(User, on_delete = models.CASCADE)
    post = models.ForeignKey(Post, on_delete = models.CASCADE)
    class Meta:
        db_table = "comment"