from django.urls import path,include
# import article.urls, post.urls

from article import views 

urlpatterns = [
    path('', views.showAllArticles, name='articles')
]