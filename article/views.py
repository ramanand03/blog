from django.shortcuts import render, redirect
from .models import Article
from django.contrib.auth.decorators import login_required


def showAllArticles(request):    
    article = Article.objects.all()
    return render(request, 'article/allarticle.html', {'article':article, 'index':0, 'title':'Articles', 'allarticle_button':'active'})

@login_required
def createArticle(request):  
    if request.method == 'POST':
        Article(title = request.POST['article_title'], desc = request.POST['article_desc'], user_id = request.user.id).save()
        return redirect('/articles' )
    return render(request, 'article/create-article.html', {'create_post_displayflag':'none', 'title':'Create Article'})

