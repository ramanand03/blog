from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Article(models.Model):
    id = models.AutoField( primary_key=True)
    title = models.CharField(max_length=30)
    desc = models.CharField(max_length = 100)
    user = models.ForeignKey(User, on_delete = models.CASCADE)
    class Meta:
        db_table = "article"